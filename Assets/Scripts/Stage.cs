﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage : MonoBehaviour {

	void OnTriggerExit(Collider other)
	{
		PooledObject victim = other.GetComponent<PooledObject> ();
		if ( victim != null) {
			Pools.KillObject (victim);
		}
	}


}
