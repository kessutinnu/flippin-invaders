﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenOverlay : MonoBehaviour {

    protected LevelManagement levelManagement;

    // Use this for initialization
    void Start()
    {
        levelManagement = FindObjectOfType<LevelManagement>();
    }

    public void Activate()
    {
        gameObject.SetActive(true);
    }

    public void Deactivate()
    {
        gameObject.SetActive(false);
    }

}