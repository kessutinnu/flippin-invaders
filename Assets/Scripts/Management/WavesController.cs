﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WavesController : MonoBehaviour {

    private int waveCount;
    private int enemyCount;

    public void AddWave() {
        waveCount += 1;
    }

    public void WaveSpawned() {
        waveCount -= 1;
    }

    public void AddEnemy()
    {
        enemyCount += 1;
    }

    public void EnemyDied() {
        enemyCount -= 1;
        if (enemyCount == 0 && waveCount == 0) {
            FindObjectOfType<LevelManagement> ().Win();
        }
    }
}
