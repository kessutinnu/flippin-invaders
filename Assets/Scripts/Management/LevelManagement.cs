﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManagement : MonoBehaviour {

    private PauseScreen pauseScreen;
    private EndScreen endScreen;
    private InputHandler player;

    void Start() {
        pauseScreen = GetComponentInChildren<PauseScreen>(true);
        endScreen = GetComponentInChildren<EndScreen>(true);
        player = FindObjectOfType<InputHandler>();
    }

	public void QuitGame(){
		Application.Quit ();
	}

	public void LoadScene(int index){
        if (index != 0)//not main menu
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        Time.timeScale = 1;
        SceneManager.LoadScene (index);
	}

    public void NextScene() {
        LoadScene(SceneManager.GetActiveScene().buildIndex+1);       
    }

	public void Restart(){
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Time.timeScale = 1;
        SceneManager.LoadScene (SceneManager.GetActiveScene().name);
	}

    public void Pause()
    {
        pauseScreen.Activate();
        Time.timeScale = 0; //stop fixed updates
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void Resume()
    {
        pauseScreen.Deactivate();
        player.enabled = true;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Time.timeScale = 1;
    }

    public void Lose(){
        endScreen.Activate();
		Time.timeScale = 0; //stop fixed updates
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
	}

    public void Win()
    {
        endScreen.ActivateWithNextLevelButton();
        Time.timeScale = 0; //stop fixed updates
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }


}
