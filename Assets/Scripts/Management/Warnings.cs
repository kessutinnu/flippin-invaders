﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Warnings : MonoBehaviour {

    private Image[] sides;
	// Use this for initialization
	void Start () {
        sides = GetComponentsInChildren<Image>(true);
	}
    public void SetWarning(Wave.Direction dir, bool showWarning)
    {
        if (dir == Wave.Direction.down)
            sides[0].gameObject.SetActive(showWarning);
        else if (dir == Wave.Direction.right)
            sides[1].gameObject.SetActive(showWarning);
        else if (dir == Wave.Direction.left)
            sides[2].gameObject.SetActive(showWarning);
        else if (dir == Wave.Direction.up)
            sides[3].gameObject.SetActive(showWarning);

    }

}
