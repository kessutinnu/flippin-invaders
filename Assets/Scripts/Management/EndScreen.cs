﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class EndScreen : ScreenOverlay
{
    public void ActivateWithNextLevelButton() {
        GetComponentInChildren<Button>().interactable = true; //first button
        gameObject.SetActive(true);
    }
}