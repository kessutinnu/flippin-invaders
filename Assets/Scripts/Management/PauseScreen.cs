﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseScreen : ScreenOverlay{
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            levelManagement.Resume();
            Deactivate();
        }
    }


}
