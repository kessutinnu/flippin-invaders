﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/** causes player to die when hit by enemy */
public class PlayerHealth : MonoBehaviour {

	void OnTriggerEnter (Collider col)
	{
		print (col);
		if(col.gameObject.GetComponent<EnemyHealth>() != null)
		{
            gameObject.GetComponent<InputHandler>().enabled = false;
			FindObjectOfType<LevelManagement>().Lose ();
		}
	}


}
