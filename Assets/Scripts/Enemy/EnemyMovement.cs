﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{

    // private List<Vector3> directions = new List<Vector3> { Vector3.right, Vector3.down, Vector3.left, Vector3.down };
    private int actualDirectionIndex = 0;

    private Vector3 movementDirection;

    private float initialTime;
    public float speed = 0.5f;

    // Use this for initialization
    void Start()
    {
        actualDirectionIndex = 0;
        initialTime = Time.time;
        InvokeRepeating("FollowPlayer", 2, 2.0F);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.Translate(this.movementDirection * speed * Time.deltaTime);
        // if (Time.time - initialTime <= MovementTime)
        // {
        //     transform.Translate(directions[actualDirectionIndex] * Speed * Time.deltaTime);
        // }
        // else
        // {
        //     initialTime = Time.time;
        //     actualDirectionIndex = (actualDirectionIndex + 1) % directions.Count;
        // }
    }

    private void FollowPlayer()
    {
        InputHandler player = FindObjectOfType<InputHandler>();
        Vector3 playerPosition = player.transform.position;

        if (movementDirection.x != 0)
        {
            if ((playerPosition.x >= transform.position.x && movementDirection == Vector3.left) || (playerPosition.x <= transform.position.x && movementDirection == Vector3.right))
            {
                movementDirection = playerPosition.y < transform.position.y ? Vector3.down : Vector3.up;
            }
        }
        else
        {
            if ((playerPosition.y >= transform.position.y && movementDirection == Vector3.down) || (playerPosition.y <= transform.position.y && movementDirection == Vector3.up))
            {
                movementDirection = playerPosition.x < transform.position.x ? Vector3.left : Vector3.right;
            }
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.GetComponent<EnemyMovement>() != null)
        {
            convertEnemy(col.gameObject);
        }
    }

    private void convertEnemy(GameObject enemyObject)
    {
        PooledObject currentEnemy = gameObject.GetComponent<PooledObject>();
        PooledObject colliderEnemy = enemyObject.GetComponent<PooledObject>();
        if (currentEnemy.isOlderThan(colliderEnemy))
        {
            this.movementDirection = enemyObject.GetComponent<EnemyMovement>().GetMovementDirection();
        }
        else
        {
            enemyObject.GetComponent<EnemyMovement>().SetMovementDirection(this.movementDirection);
        }
        currentEnemy.SetCreationTime(Time.time);
    }

    public void SetMovementDirection(Vector3 newDirection)
    {
        this.movementDirection = newDirection;
    }

    public Vector3 GetMovementDirection()
    {
        return this.movementDirection;
    }

}
