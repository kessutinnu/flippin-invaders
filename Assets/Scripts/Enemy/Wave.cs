﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wave : MonoBehaviour
{
	/** Strings where '*' means enemy unit and anything else means empty. */
	[SerializeField] private List<string> lines;
	[SerializeField] int goTime;
	[SerializeField] Direction startDirection;
	public GameObject enemy;
	private float movementTime;
    private Vector3 directionVector;
    private WavesController waves;
    private Warnings warnings;

    public enum Direction
	{
left,
right,
up,
down
	
	}

	// Use this for initialization
	void Start ()
	{
        waves = FindObjectOfType<WavesController>();
        warnings = FindObjectOfType<Warnings>();
        waves.AddWave();
		movementTime = 1/enemy.GetComponent<EnemyMovement> ().speed;
        Invoke("Warn", Mathf.Max(0,goTime-1));
        Invoke ("Launch", goTime);
	}

	private void Launch ()
	{
        warnings.SetWarning(startDirection, false);
        Vector3 currentPos;
		Vector3 posIncrement;
		switch (startDirection) {
            case Direction.left:
                currentPos = new Vector3(5.5f, 4.5f);
                posIncrement = new Vector3(0f, -1f);
                directionVector = Vector3.left;
			break;
		case Direction.right:
                currentPos = new Vector3(-5.5f, 4.5f);
                posIncrement = new Vector3(0f, -1f);
                directionVector = Vector3.right;
                break;
		case Direction.up:
			currentPos = new Vector3 (-4.5f, -5.5f);
			posIncrement = new Vector3 (1f, 0f);
                directionVector = Vector3.up;
                break;
		default:
			currentPos = new Vector3 (-4.5f, 5.5f);
			posIncrement = new Vector3 (1f, 0f);
                directionVector = Vector3.down;
                break;
		}
		foreach(char symbol in lines[0]){
			if(symbol == '*'){
				EnemyMovement enemyInstance = Pools.GetObject (enemy).instance.GetComponent<EnemyMovement>();
				enemyInstance.transform.position = currentPos;
                enemyInstance.SetMovementDirection(directionVector);
                waves.AddEnemy();
			}
			currentPos += posIncrement;
		}
		lines.RemoveAt(0);
        if (lines.Count > 0)
        {
            Invoke("Launch", movementTime);
        }
        else {
            waves.WaveSpawned();
        }
	}

    private void Warn() {
        warnings.SetWarning(startDirection, true);
    }
}
