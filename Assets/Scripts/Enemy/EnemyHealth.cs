﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {

	public int totalHealth=1;
	private int health;

	void Start(){
		health = totalHealth;
	}

	void OnTriggerEnter (Collider col)
	{
		//print (col);
		if(col.gameObject.GetComponent<Bullet>() != null)
		{
			health -= 1;
			Pools.KillObject (col.gameObject.GetComponent<PooledObject> ());
			if (health == 0) {
				health = totalHealth;
                FindObjectOfType<WavesController>().EnemyDied();
                Pools.KillObject (gameObject.GetComponent<PooledObject>());               
			}
		}
	}
}
