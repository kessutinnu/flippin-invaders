﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool {

	private List<PooledObject> objects = new List<PooledObject>();
	private int index = 0;

	public Pool(GameObject prefab, int count){
		GameObject poolParent = new GameObject (prefab.name+" Pool");
		for (int i = 0; i < count; i++) {
			GameObject instance = Object.Instantiate<GameObject> (prefab,poolParent.transform);
			PooledObject po = instance.AddComponent<PooledObject> ();
			po.Create (instance, prefab, i);
			objects.Add (po);
		}
	}
	
	public PooledObject GetPooledObject(){
		objects [index].Live ();
		return objects [index++];
	}

	public void KillPooledObject(PooledObject victim){
		int dying = victim.index;
		if (dying != --index) {
			objects [dying] = objects [index];
			objects [dying].index = victim.index;
			victim.index = index;
			objects [index] = victim;
		}
		victim.Die ();
	}
}
