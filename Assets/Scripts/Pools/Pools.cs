﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pools : MonoBehaviour {

	static Dictionary<GameObject,Pool> objectPools = new Dictionary<GameObject,Pool>();

	void Start () {
        objectPools.Clear();
        GameObject ammo = FindObjectOfType<InputHandler>().ammo;
        objectPools.Add(ammo, new Pool(ammo, 6));

        foreach(Wave v in FindObjectsOfType<Wave>()) {
            GameObject enemy = v.enemy;
            if(!objectPools.ContainsKey(enemy))
                objectPools.Add(enemy, new Pool(enemy, 99));
        }

	}
	
	public static PooledObject GetObject(GameObject key){
		return objectPools [key].GetPooledObject ();
	}

	public static void KillObject(PooledObject victim){
		objectPools [victim.original].KillPooledObject (victim);
	}
		
}
