﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PooledObject : MonoBehaviour {

	public GameObject instance{ get; set;}
	public GameObject original{ get; set; }
	public int index{ get; set;}

	private float creationTime;


	public PooledObject(GameObject poolable, GameObject prefab, int i){
		instance = poolable;
		original = prefab;
		index = i;
		instance.SetActive (false);
	}

	public void Create (GameObject poolable, GameObject prefab, int i){
		instance = poolable;
		original = prefab;
		index = i;
		
		instance.SetActive (false);
	}

	public bool isOlderThan(PooledObject other) {
		return this.creationTime < other.creationTime;
	}

	public void SetCreationTime(float time) {
		this.creationTime = time;
	}

	public void Live(){
		creationTime = Time.time;
		instance.SetActive (true);
	}

	public void Die(){
		instance.SetActive (false);
	}
}
