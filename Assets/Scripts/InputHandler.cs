﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour {

	[SerializeField] float maxSpeed;
    public GameObject ammo;
	[SerializeField] float cooldown;
	private AudioSource shootSoundFX;					        // The player shooting sound effect
	private Direction shootDirection;
	private enum Direction{horizontal, vertical}
	private bool flipped;
	private float cooldownProgress;
    private LevelManagement levelManagement;

	// Use this for initialization
	void Start () {
		shootDirection = Direction.vertical;
		flipped = false;
		cooldownProgress = 0;
		shootSoundFX = gameObject.GetComponent<AudioSource>();
        levelManagement = FindObjectOfType<LevelManagement>();
	}

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            levelManagement.Pause();
            this.enabled = false;
        }
        //handle movement
        float move;
        if (shootDirection == Direction.vertical)
        {
            move = Input.GetAxis("Horizontal") * maxSpeed * Time.deltaTime;
            transform.position += new Vector3(move, 0f);
        }
        else {
            move = Input.GetAxis("Vertical") * maxSpeed * Time.deltaTime;
            transform.position += new Vector3(0f, move);
        }
        if (Input.GetAxisRaw("Flip") == 1 && !flipped)
        {
            flipped = true;
            transform.rotation = Quaternion.LookRotation(transform.forward, -1 * transform.up);
        }
        //handle rotating
        float dir = Input.GetAxisRaw(shootDirection == Direction.vertical ? "Vertical": "Horizontal");
        if (move == 0 && dir != 0)
        {
            transform.rotation = Quaternion.LookRotation( transform.forward, shootDirection == Direction.vertical ?  new Vector3(1f, 0) * dir : new Vector3(0, 1f) * dir);
            shootDirection = shootDirection == Direction.vertical ? Direction.horizontal : Direction.vertical;
        }

		flipped = Input.GetAxisRaw ("Flip")==1;

		if (Input.GetAxis ("Fire") != 0 && cooldownProgress <= 0) {
			cooldownProgress = cooldown;
			shootSoundFX.Play();
			Bullet bullet = Pools.GetObject(ammo).instance.GetComponent<Bullet>();
			bullet.fire (transform.position + transform.up, transform.up);
		} else if (cooldownProgress > 0) {
			cooldownProgress -= Time.deltaTime;
		}

	}
}
