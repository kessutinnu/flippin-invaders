﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	Vector3 direction;
	[SerializeField] float speed;

	public void fire(Vector3 startPos, Vector3 dir){
		direction = dir;
		transform.position = startPos;
	}

	// Update is called once per frame
	void FixedUpdate () {
		transform.position+= direction*speed*Time.deltaTime;
	}
}
